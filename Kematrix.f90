module Kematrix
  use module
  use matrix2
  implicit none
  integer o, q, r

  contains

    subroutine Kesub(a, c, d, bdbdetp, Ke, globalnum, n)
      integer,allocatable::globalnum(:,:)
      real(8)::  a, c
      real(8),allocatable::n(:,:)
  real(8):: Ke(8,8), bdbdetp(8,8), d(3,3)
  real(8):: bdbdet1(8,8) = 0, bdbdet2(8,8) = 0, bdbdet3(8,8) = 0, bdbdet4(8,8) = 0
  
  a = -0.57735d0
  c = -0.57735d0

    call sub(a, c, n, d, bdbdetp, globalnum)

    do q = 1, 8
       do o = 1, 8
          bdbdet1(o,q) = bdbdetp(o,q)
       end do
    end do
    
  a = -0.57735d0
  c = 0.57735d0

  call sub(a, c, n, d, bdbdetp, globalnum)

  do q = 1, 8
     do o = 1, 8
        bdbdet2(o,q) = bdbdetp(o,q)
     end do
  end do
  
    a = 0.57735d0
    c = 0.57735d0
    call sub(a, c, n, d, bdbdetp, globalnum)

    do q = 1, 8
       do o = 1, 8
          bdbdet3(o,q) = bdbdetp(o,q)
       end do
    end do

a = 0.57735d0
c = -0.57735d0

call sub(a, c, n, d, bdbdetp, globalnum)

do q = 1, 8
   do o = 1, 8
      bdbdet4(o,q) = bdbdetp(o,q)
   end do
end do     


  Ke = bdbdet1 + bdbdet2 + bdbdet3 + bdbdet4 !Ke1計算

!do o = 1, 8
!  write(*,*) Ke1(o,1), Ke1(o,2), Ke1(o,3), Ke1(o,4), Ke1(o,5), Ke1(o,6), Ke1(o,7), Ke1(o,8)
!end do

end subroutine Kesub
  
subroutine sub(a, c, n, d, bdbdetp, globalnum)
  integer,allocatable::globalnum(:,:)
    real(8):: a, c, det
    real(8):: bdbdetp(8,8), bdb(8,8) = 0, db(3,8), jm1(2,2), bm1(3,8) = 0, bt(8,3) = 0, d(3,3)
    real(8), allocatable ::n(:,:)
    
    jm1 = jm(a, c, n, globalnum) !関数jm1（2，2）を定義

    bm1 = bm(a, c, jm1) !関数bm1（3，8）を定義
    
    do o = 1, 3
       do q = 1, 8
          bt(q,o) = bm1(o,q) !btを定義
       end do
    end do

    do o = 1, 3
       do q = 1, 8
          do r = 1, 3
             db(o,q) = db(o,q) + d(o,r) * bm1(r,q) !dbを計算
          end do
       end do
    end do

    do o = 1, 8
       do q = 1, 8
          do r = 1, 3
             bdb(o,q) = bdb(o,q) + bt(o,r) * db(r,q) !bdbを計算
          end do
       end do
    end do

    det = (jm1(1,1) * jm1(2,2)) - (jm1(1,2) * jm1(2,1)) !det計算

    do o = 1, 8
       do q = 1, 8
          bdbdetp(q,o) = bdb(q,o) * det !bdbdetp計算
       end do
    end do

    
  end subroutine sub
      
end module Kematrix
