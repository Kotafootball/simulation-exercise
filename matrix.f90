module matrix
  use module
  implicit none
  real(8):: a = 0.0d0, c = 0.0d0
  real(8):: j(2,2), b(3,8), d(3,3), bt(8,3)
  real(8)::x1, x2, x3, x4, y1, y2, y3, y4
contains

  subroutine detail
    implicit none

    real(8)::  det, n1x, n2x, n3x, n4x, n1y, n2y, n3y, n4y
    integer l, m

    call read_file1
    call read_file2
    call read_file3
    
    x1 = n(1,1)
    x2 = n(2,1)
    x3 = n(5,1)
    x4 = n(4,1)
    y1 = n(1,2)
    y2 = n(2,2)
    y3 = n(5,2)
    y4 = n(4,2)
    
    j(1,1) = ((-1.0d0 + a) * x1 +(1.0d0 - a) * x2 +(1.0d0 + a) * x3 + (-1.0d0 - a) * x4) / 4.0d0
    j(2,1) = ((-1.0d0 + c) * x1 +(-1.0d0 - c) * x2 +(1.0d0 + c) * x3 + (1.0d0 - c) * x4) / 4.0d0
    j(1,2) = ((-1.0d0 + a) * y1 +(1.0d0 - a) * y2 +(1.0d0 + a) * y3 + (-1.0d0 - a) * y4) / 4.0d0
    j(2,2) = ((-1.0d0 + c) * y1 +(-1.0d0 - c) * y2 +(1.0d0 + c) * y3 + (1.0d0 - c) * y4) / 4.0d0

    n1x = -(1.0d0 - a) / 4.0d0 / j(1,1) - (1.0d0 - c) / 4.0d0 / j(1,2)
    n1y = -(1.0d0 - a) / 4.0d0 / j(2,1) - (1.0d0 - c) / 4.0d0 / j(2,2)
    n2x = (1.0d0 - a) / 4.0d0 / j(1,1) - (1.0d0 + c) / 4.0d0 / j(1,2)
    n2y = (1.0d0 - a) / 4.0d0 / j(2,1) - (1.0d0 + c) / 4.0d0 / j(2,2)
    n3x = (1.0d0 + a) / 4.0d0 / j(1,1) + (1.0d0 + c) / 4.0d0 / j(1,2)
    n3y = (1.0d0 + a) / 4.0d0 / j(2,1) + (1.0d0 + c) / 4.0d0 / j(2,2)
    n4x = -(1.0d0 + a) / 4.0d0 / j(1,1) + (1.0d0 - c) / 4.0d0 / j(1,2)
    n4y = -(1.0d0 + a) / 4.0d0 / j(2,1) + (1.0d0 - c) / 4.0d0 / j(2,2)

    b(1,1) = n1x
    b(1,2) = 0.0d0
    b(1,3) = n2x
    b(1,4) = 0.0d0
    b(1,5) = n3x
    b(1,6) = 0.0d0
    b(1,7) = n4x
    b(1,8) = 0.0d0
    b(2,1) = 0.0d0
    b(2,2) = n1y
    b(2,3) = 0.0d0
    b(2,4) = n2y
    b(2,5) = 0.0d0
    b(2,6) = n3y
    b(2,7) = 0.0d0
    b(2,8) = n4y
    b(3,1) = n1y
    b(3,2) = n1x
    b(3,3) = n2y
    b(3,4) = n2x
    b(3,5) = n3y
    b(3,6) = n3x
    b(3,7) = n4y
    b(3,8) = n4x

    do l = 1, 3
       do m = 1, 8
          bt(m,l) = b(l,m)
       end do
    end do

    d(1,1) = e / (1.0d0 + p) / (1.0d0 - p)
    d(1,2) = e / (1.0d0 + p) * p / (1.0d0 - p)
    d(1,3) = 0.0d0
    d(2,1) = e / (1.0d0 + p) * p / (1.0d0 - p)
    d(2,2) = e / (1.0d0 + p) / (1.0d0 - p)
    d(2,3) = 0.0d0
    d(3,1) = 0.0d0
    d(3,2) = 0.0d0
    d(3,3) = 1.0d0 / 2.0d0
  end subroutine detail
  
end module matrix
