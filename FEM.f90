program main
use module
implicit none

integer j, Nn, iter, l, m, x1, bc0, f1, pn, e, ep, ld0, f2, g
integer,allocatable::globalnum(:,:),bc(:,:),ld(:,:)
real(8), allocatable::u(:), f(:), Rr(:),Pp(:), Qq(:), K(:,:),n(:,:),bc1(:),ldf(:),eps(:,:),sgm(:,:)
real(8) ::bm(3,8) = 0.0d0,d(3,3) = 0.0d0,jm(2,2) = 0.0d0,jmi(2,2) = 0.0d0,nxy(4,2) = 0.0d0,Ke(8,8) = 0.0d0
real(8) BNRM2, RHO, BETA, RHO1, C1, ALPHA, DNRM2, RESID, EPSRN, y, p,a,c

  call read_file1(y, p) 
  call read_file2(pn, n)
  call read_file3(e, ep, globalnum) 
  call read_file4(bc0 ,f1, bc, bc1) 
  call read_file5(ld0,f2,ld,ldf)
 allocate(K(pn * f1,pn * f1), source = 0.0d0)
do j = 1, e
    call Kesub(j,globalnum, n, y,p,d,jm,jmi,bm,Ke)
    call Kmat(j,globalnum,f1,pn,ep,bc0,bc,Ke,K)
end do

Nn = pn * f1
allocate(u(Nn), f(Nn), Rr(Nn), Pp(Nn), Qq(Nn), source = 0.0d0)
allocate(eps(3,e),sgm(3,e))

!!!ディリクレ境界条件とノイマン境界条件
do l = 1, bc0
   if (bc(l,2) == 1) then
   x1 = bc(l,1) * f1 - 1
   else if (bc(l,2) == 2) then
   x1 = bc(l,1) * f1
   end if
   do m = 1, f1 * pn
      if (m == x1) then
      f(m) = bc1(l)
      end if
   end do
end do

do l = 1, ld0
   if (ld(l,2) == 1) then
   x1 = ld(l,1) * f2 - 1
   else if (ld(l,2) == 2) then
   x1 = ld(l,1) * f2
   end if
   do m = 1, f2 * pn
      if (m == x1) then
      f(m) = ldf(l)
      end if
   end do
end do

write(*,*)f(1:Nn)
!!!ここまではディリクレ境界条件とノイマン境界条件
do o = 1, pn * f1
  write(*,"(f15.5,1x,f15.5,1x,f15.5,1x,f15.5,1x,f15.5,1x,f15.5,1x,f15.5,1x,f15.5,1x,f15.5,1x,f15.5,&
  1x,f15.5,1x,f15.5,1x,f15.5,1x,f15.5,1x,f15.5,1x,f15.5,1x,f15.5,1x,f15.5)") K(o,1), &
   K(o,2),K(o,3),K(o,4),K(o,5),K(o,6),K(o,7),K(o,8),K(o,9),K(o,10),K(o,11),K(o,12),K(o,13), &
   K(o,14),K(o,15),K(o,16),K(o,17),K(o,18)
end do

!!!ここからCG法
do i= 1, Nn
Rr(i) = f(i)
    do j = 1, Nn
    Rr(i) = Rr(i) - K(i,j) * u(j) !Rrは残差
    end do
end do

BNRM2 = 0.0d0

do i= 1, Nn
    BNRM2 = BNRM2 + f(i) ** 2 !fのノルムの2乗
end do

do iter = 1, Nn
    RHO = 0.0d0
    do i= 1, Nn
        RHO = RHO + Rr(i) * Rr(i) 
    end do
 
    if ( iter == 1 ) then
    do i= 1, Nn
        Pp(i)= Rr(i)
    end do
    else
        BETA = RHO / RHO1
    do i= 1, Nn
        Pp(i) = Rr(i) + BETA * Pp(i)
    end do
    end if
    do i= 1, Nn
        Qq(i)= 0.0d0
        do j= 1, Nn
            Qq(i) = Qq(i) + K(i,j) * Pp(j)
        end do
    end do

    C1 = 0.0d0
    do i= 1, Nn
        C1 = C1 + Pp(i) * Qq(i)
    end do
    ALPHA = RHO / C1
    do i = 1, Nn
        u(i) = u(i) + ALPHA * Pp(i)
        Rr(i) = Rr(i) - ALPHA * Qq(i)
    end do
    DNRM2 = 0.0d0
    do i = 1, Nn
        DNRM2 = DNRM2 + Rr(i) ** 2 !残差のノルムの2乗
    end do
    RESID = dsqrt(DNRM2 / BNRM2)
    EPSRN = 1.0e-5
    if ( RESID <= EPSRN) exit
        RHO1 = RHO
end do 

do i = 1, pn*f1
  write(*,*) u(i)
enddo
!ここまでがCG法

call additional(e,d,globalnum,n,u,eps,sgm)

call print_text(g,ep,e,pn,globalnum,n,u,eps,sgm)

end program