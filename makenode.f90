 program main
 implicit none
 integer sp1,sp2,sp3,i,j,c
 integer(8) pn
 real(8),allocatable::x1(:,:),y1(:,:),x2(:,:),y2(:,:),bx(:),by(:)
 real(8) a,b,r
a = 6.0d0
b = 7.0d0
r = 1.0d0

sp1 = 40
sp2 = 32
sp3 = 32
c = 3 !rのc+1倍の領域で細かいメッシュ

allocate(x1((sp2 + 1),(sp2 + sp1 + 1)),source = 0.0d0)
allocate(y1((sp2 + 1),(sp2 + sp1 + 1)),source = 0.0d0)
allocate(by(sp2 + 1),source = 0.0d0)


do i = 2, sp2 + 1
by(i) = by(i - 1) + (b / sp2)
end do

do j = 1, sp2 + 1
x1(j,1) = (a / ((a ** 2 + by(j) ** 2) ** 0.5)) * r
end do

do j = 1, sp2 + 1
y1(j,1) = by(j) / ((a ** 2 + by(j) ** 2) ** 0.5) * r
end do

do j = 1, sp2 + 1
do i = 2, sp2 + 1
x1(j,i) = x1(j,i - 1) + (c * x1(j,1) / sp2)
end do
do i = sp2 + 2, sp1 + sp2 + 1
x1(j,i) = x1(j,i - 1) + ((a - (c + 1) * x1(j,1)) / sp1)
end do
end do

do j = 1, sp2 + 1
do i = 2, sp2 + 1
y1(j,i) = y1(j,i - 1) + (c * y1(j,1) / sp2)
end do
do i = sp2 + 2, sp1 + sp2 + 1
y1(j,i) = y1(j,i - 1) + ((by(j) - (c + 1) * y1(j,1)) / sp1)
end do
end do

allocate(bx(sp3),source = 0.0d0)
allocate(x2(sp3,(sp3 + sp1 + 1)),source = 0.0d0)
allocate(y2(sp3,(sp3 + sp1 + 1)),source = 0.0d0)

do i = 2, sp3
bx(i) = bx(i - 1) + (a / sp3)
end do

do j = 1, sp3
x2(j,1) = bx(j) / ((b ** 2 + bx(j) ** 2) ** 0.5) * r
end do
do j = 1, sp3
y2(j,1) = b / ((b ** 2 + bx(j) ** 2) ** 0.5) * r
end do

do j = 1, sp3
do i = 2, sp3 + 1
x2(j,i) = x2(j,i - 1) + (c * x2(j,1) / sp3)
end do
do i = sp3 + 2, sp3 + sp1 + 1
x2(j,i) = x2(j,i - 1) + ((bx(j) - (c + 1) * x2(j,1)) / sp1)
end do
end do

do j = 1, sp3
do i = 2, sp3 + 1
y2(j,i) = y2(j,i - 1) + (c * y2(j,1) / sp3)
enddo
do i = sp3 + 2, sp3 + sp1 + 1
y2(j,i) = y2(j,i - 1) + ((b - (c + 1) * y2(j,1)) / sp1)
enddo
end do

pn = (sp2 + sp1 + 1) * (sp2 + 1) + (sp3 + sp1 + 1) * sp3

    open(30, file = "node2.dat", status = "replace")
    write(30,*) pn
do i = 1, sp2 + 1
do j = 1, sp2 + sp1 + 1
write(30,*)x1(i,j),y1(i,j)
end do
end do

do i = 1, sp3
do j = 1, sp3 + sp1 + 1
write(30,*)x2(sp3 + 1 - i,j),y2(sp3 + 1 - i,j)
end do
end do

    close (30)

    end program