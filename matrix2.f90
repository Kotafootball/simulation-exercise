module matrix2
  use module
  implicit none
 
contains
   
  subroutine detail2
real(8)::d(3,3)
    call read_file1
    call read_file3

    d(1,1) = e / (1.0d0 + p) / (1.0d0 - p)
    d(1,2) = e / (1.0d0 + p) * p / (1.0d0 - p)
    d(1,3) = 0.0d0
    d(2,1) = e / (1.0d0 + p) * p / (1.0d0 - p)
    d(2,2) = e / (1.0d0 + p) / (1.0d0 - p)
    d(2,3) = 0.0d0
    d(3,1) = 0.0d0
    d(3,2) = 0.0d0
    d(3,3) = 1.0d0 / 2.0d0

  end subroutine detail2

  function bm(a, c, l)
    integer l
    real(8)::jm(2,2), jmi(2,2), nxy(4,2), bm(3,8)
    real(8), allocatable :: n(:,:)
    real(8), intent(in)::a, c
    call read_file2
    call read_file3
    
    jm(1,1) = ((-1.0d0 + a) * n(globalnum(l,1),1) +(1.0d0 - a) * n(globalnum(l,2),1) +&
    (1.0d0 + a) * n(globalnum(l,3),1) + (-1.0d0 - a) * n(globalnum(l,4),1)) / 4.0d0
    jm(2,1) = ((-1.0d0 + c) * n(globalnum(l,1),1) +(-1.0d0 - c) * n(globalnum(l,2),1) +&
    (1.0d0 + c) * n(globalnum(l,3),1) + (1.0d0 - c) * n(globalnum(l,4),1)) / 4.0d0
    jm(1,2) = ((-1.0d0 + a) * n(globalnum(l,1),2) +(1.0d0 - a) * n(globalnum(l,2),2) +&
    (1.0d0 + a) * n(globalnum(l,3),2) + (-1.0d0 - a) * n(globalnum(l,4),2)) / 4.0d0
    jm(2,2) = ((-1.0d0 + c) * n(globalnum(l,1),2) +(-1.0d0 - c) * n(globalnum(l,2),2) +&
    (1.0d0 + c) * n(globalnum(l,3),2) + (1.0d0 - c) * n(globalnum(l,4),2)) / 4.0d0

    jmi(1,1) = jm(2,2) / (jm(1,1) * jm(2,2) - jm(1,2) * jm(2,1))
    jmi(1,2) = -jm(1,2) / (jm(1,1) * jm(2,2) - jm(1,2) * jm(2,1))
    jmi(2,1) = -jm(2,1) / (jm(1,1) * jm(2,2) - jm(1,2) * jm(2,1))
    jmi(2,2) = jm(1,1) / (jm(1,1) * jm(2,2) - jm(1,2) * jm(2,1))

    nxy(1,1) = -(1.0d0 - a) / 4.0d0 / jmi(1,1)
    nxy(1,2) = - (1.0d0 - c) / 4.0d0 / jmi(2,2)
    nxy(2,1) = (1.0d0 - a) / 4.0d0 / jmi(1,1)
    nxy(2,2) = - (1.0d0 + c) / 4.0d0 / jmi(2,2)
    nxy(3,1) = (1.0d0 + a) / 4.0d0 / jmi(1,1)
    nxy(3,2) = (1.0d0 + c) / 4.0d0 / jmi(2,2)
    nxy(4,1) = -(1.0d0 + a) / 4.0d0 / jmi(1,1) 
    nxy(4,2) = (1.0d0 - c) / 4.0d0 / jmi(2,2)
        
        bm(1,1) = nxy(1,1)
        bm(1,2) = 0.0d0
        bm(1,3) = nxy(2,1)
        bm(1,4) = 0.0d0
        bm(1,5) = nxy(3,1)
        bm(1,6) = 0.0d0
        bm(1,7) = nxy(4,1)
        bm(1,8) = 0.0d0
        bm(2,1) = 0.0d0
        bm(2,2) = nxy(1,2)
        bm(2,3) = 0.0d0
        bm(2,4) = nxy(2,2)
        bm(2,5) = 0.0d0
        bm(2,6) = nxy(3,2)
        bm(2,7) = 0.0d0
        bm(2,8) = nxy(4,2)
        bm(3,1) = nxy(1,2)
        bm(3,2) = nxy(1,1)
        bm(3,3) = nxy(2,2)
        bm(3,4) = nxy(2,1)
        bm(3,5) = nxy(3,2)
        bm(3,6) = nxy(3,1)
        bm(3,7) = nxy(4,2)
        bm(3,8) = nxy(4,1)

      end function bm
    end module matrix2
