program main
use module
implicit none
 character dummy
integer e,ep
integer,allocatable::globalnum(:,:)
integer(8) pn
real(8) y1,y2,p1,p2
real(8),allocatable::x(:),y(:),r(:)

y1 = 10000.0d0
p1 = 0.25d0
y2 = 100000.0d0
p2 = 0.3d0

call gmshtofile(y1,y2,p1,p2,pn,e,ep,globalnum,x,y,r)

end program