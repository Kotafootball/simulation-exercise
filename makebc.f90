program main
 implicit none
 integer sp1,sp2,sp3,i,j,c,d,bc0
 integer(8) pn
 real(8),allocatable::x1(:,:),y1(:,:),x2(:,:),y2(:,:),bx(:),by(:), ux(:),uy(:)
 real(8) a,b,r,rr,sgm0,y,p,sin3,cos3,mu,k,sita,si,co

y = 20000.0d0
p = 0.3d0

a = 6.0d0
b = 7.0d0
r = 1.0d0

sp1 = 40
sp2 = 32
sp3 = 32
c = 3   ! rのc+1倍の領域で細かいメッシュ

sgm0 = 10000

allocate(x1((sp2 + 1),(sp2 + sp1 + 1)),source = 0.0d0)
allocate(y1((sp2 + 1),(sp2 + sp1 + 1)),source = 0.0d0)
allocate(by(sp2 + 1),source = 0.0d0)


do i = 2, sp2 + 1
by(i) = by(i - 1) + (b / sp2)
end do

do j = 1, sp2 + 1
x1(j,1) = (a / ((a ** 2 + by(j) ** 2) ** 0.5)) * r
end do

do j = 1, sp2 + 1
y1(j,1) = by(j) / ((a ** 2 + by(j) ** 2) ** 0.5) * r
end do

do j = 1, sp2 + 1
do i = 2, sp2 + 1
x1(j,i) = x1(j,i - 1) + (c * x1(j,1) / sp2)
end do
do i = sp2 + 2, sp1 + sp2 + 1
x1(j,i) = x1(j,i - 1) + ((a - (c + 1) * x1(j,1)) / sp1)
end do
end do

do j = 1, sp2 + 1
do i = 2, sp2 + 1
y1(j,i) = y1(j,i - 1) + (c * y1(j,1) / sp2)
end do
do i = sp2 + 2, sp1 + sp2 + 1
y1(j,i) = y1(j,i - 1) + ((by(j) - (c + 1) * y1(j,1)) / sp1)
end do
end do

allocate(bx(sp3),source = 0.0d0)
allocate(x2(sp3,(sp3 + sp1 + 1)),source = 0.0d0)
allocate(y2(sp3,(sp3 + sp1 + 1)),source = 0.0d0)

do i = 2, sp3
bx(i) = bx(i - 1) + (a / sp3)
end do

do j = 1, sp3 
x2(j,1) = bx(j) / ((b ** 2 + bx(j) ** 2) ** 0.5) * r
end do
do j = 1, sp3
y2(j,1) = b / ((b ** 2 + bx(j) ** 2) ** 0.5) * r
end do

do j = 1, sp3 
do i = 2, sp3 + 1
x2(j,i) = x2(j,i - 1) + (c * x2(j,1) / sp3)
end do
do i = sp3 + 2, sp3 + sp1 + 1
x2(j,i) = x2(j,i - 1) + ((bx(j) - (c + 1) * x2(j,1)) / sp1)
end do
end do

do j = 1, sp3
do i = 2, sp3 + 1
y2(j,i) = y2(j,i - 1) + (c * y2(j,1) / sp3)
enddo
do i = sp3 + 2, sp3 + sp1 + 1
y2(j,i) = y2(j,i - 1) + ((b - (c + 1) * y2(j,1)) / sp1)
enddo
end do

!bc
d = sp2 + sp1 + 1

allocate(ux(sp2 + sp3 + 1),source = 0.0d0)
allocate(uy(sp2 + sp3 + 1),source = 0.0d0)

do i = 1, sp2 + 1
rr = sqrt(x1(i,d)*x1(i,d) + y1(i,d)*y1(i,d))
sita = atan2(y1(i,d), x1(i,d))
k = 3 - 4 * p
mu = y / 2 / (1 + p)

ux(i) = (sgm0 * r / 8 / mu) * ((2 * r / rr + rr / r) * (1 + k) * cos(sita) + 2 * &
r / rr *(1 - ((r ** 2) / (rr ** 2))) * cos(3 * sita))
uy(i) = (sgm0 * r / 8 / mu) * ((2 * r / rr - rr / r) * (1 - k) * sin(sita) + 2 * &
r / rr *(1 - ((r ** 2) / (rr ** 2))) * sin(3 * sita))
 enddo

d = sp3 + sp1 + 1
do i = 1, sp3
rr = sqrt(x2(i,d)*x2(i,d) + y2(i,d)*y2(i,d))
sita = atan2(y2(i,d), x2(i,d))
k = 3 - 4 * p
mu = y / 2 / (1 + p)
ux(sp3 + sp2 + 2 - i) = (sgm0 * r / 8 / mu) * ((2 * r / rr + rr / r) * (1 + k) * &
cos(sita) + 2 * r / rr *(1 - ((r ** 2) / (rr ** 2))) * cos(3 * sita))
uy(sp3 + sp2 + 2 - i) = (sgm0 * r / 8 / mu) * ((2 * r / rr - rr / r) * (1 - k) * &
sin(sita) + 2 * r / rr *(1 - ((r ** 2) / (rr ** 2))) * sin(3 * sita))
enddo

do i = 1, sp3 + sp2 + 1
 write(*,*) ux(i),uy(i)
enddo

bc0 = sp2 + sp3 + sp1 * 2 + (sp2 + sp3 + 1) * 2

open(30, file = "bc2.dat", status = "replace")
    write(30,*) bc0, 2
do j = 1, sp2 + sp3 + 1
write(30,*) (sp1+sp2 +1) * j, 1, ux(j)
write(30,*) (sp1+sp2 +1) * j, 2, uy(j)
end do

do i = 1, sp1 + sp2
write(30,*)i,2, 0.0d0
enddo

do i = (sp1 + sp2 + 1) * (sp2 + 1) + (sp3 + sp1 + 1) * (sp3-1) + 1, (sp1 + sp2 + 1) * (sp2 + 1) + (sp3 + sp1 + 1) * sp3 -1
write(30,*)i,1, 0.0d0
enddo
close (30)

end program
