program main
  use module
  implicit none
  integer pn,e,ep,bc0,f1,ld0,f2,g
  integer,allocatable::globalnum(:,:),bc(:,:),ld(:,:)
  real(8) y,p
  real(8),allocatable::n(:,:),bc1(:),ldf(:)
  
  call read_file1(y,p)
  call read_file2(pn,n)
  call read_file3(e,ep,globalnum)
  call read_file4(bc0,f1,bc,bc1)
  call read_file5(ld0,f2,ld,ldf)
  call print_text(g,ep,e,pn,globalnum,n)

end program main
