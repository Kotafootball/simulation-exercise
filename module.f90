module module
  implicit none
  integer i, o, q, w

contains
  
  subroutine read_file1(y,p)
    implicit none
    real(8) y, p
    open(10, file="input.dat", status="old")
    read (10, *) y, p
    close (10)
  end subroutine read_file1
  
  subroutine read_file2(pn,n)
    implicit none
    integer pn
    real(8),allocatable::n(:,:)
    open(11, file="nodeinclusion.dat", status="old")
    read (11, *) pn
    allocate(n(pn,2))
    do i = 1, pn
       read(11,*) n(i,1), n(i,2)
    end do
    close (11)
  end subroutine read_file2

  subroutine read_file3(e,ep,globalnum)
    implicit none
    integer e, ep
    integer,allocatable::globalnum(:,:)
    open(12, file="eleminclusion.dat", status="old")
    read (12, *)e, ep
    allocate (globalnum(e,ep), source = 0)
    do i = 1, e
       read(12,*)globalnum(i,1), globalnum(i,2), globalnum(i,3),globalnum(i,4)
    end do
    close (12)
  end subroutine read_file3

  subroutine read_file4(bc0,f1,bc,bc1)
    implicit none
    integer bc0, f1
    integer,allocatable::bc(:,:)
    real(8),allocatable::bc1(:)
    open(13, file="bcinclusion.dat", status="old")
    read (13, *) bc0, f1
    allocate(bc(bc0,2), source = 0)
    allocate(bc1(bc0), source = 0.0d0)
    do i = 1, bc0
       read (13, *)bc(i,1), bc(i,2), bc1(i)
    end do
    close (13)
  end subroutine read_file4

  subroutine read_file5(ld0,f2,ld,ldf)
    implicit none
    integer ld0, f2
    integer,allocatable::ld(:,:)
    real(8),allocatable::ldf(:)
    open(14, file="load2.dat", status="old")
    read (14, *)ld0, f2
    allocate(ld(ld0,2), source = 0)
    allocate(ldf(ld0), source = 0.0d0)
    do i = 1, ld0
       read(14, *) ld(i,1), ld(i,2), ldf(i)
    end do
    close (14)
  end subroutine read_file5

  subroutine print_text(g,ep,e,pn,globalnum,n,u,eps,sgm)!,el2)
    implicit none
    integer g, ep, e
    integer pn
    integer,allocatable::globalnum(:,:)
    real(8),allocatable::n(:,:),u(:),eps(:,:),sgm(:,:),el2(:)
    g = (1 + ep) * e
    open(30, file = "exercise.vtk", status = "replace")
    write(30,"(a)")"# vtk DataFile Version 4.1"
    write(30,"(a)")"this is command line"
    write(30,"(a)")"ASCII"
    write(30,"(a)")"DATASET UNSTRUCTURED_GRID"
    write(30,*)" "
    write(30,"(a,i8,1x,a)")trim("POINTS"), pn, trim("float")
    do i =1, pn
       write(30,"(f15.5,1x,f15.5,1x,f15.5)") n(i,1),n(i,2), 0.0
    end do
    write(30,*)" "
    write(30,"(a,i8,i8)")trim("CELLS"),e,g
    do i = 1, e
       write(30,*)ep,globalnum(i,1) - 1, globalnum(i,2) - 1, globalnum(i,3) - 1, globalnum(i,4) - 1
    end do
    write(30,*)" "
    write(30,"(a,i8)")trim("CELL_TYPES"),e
    do i = 1, e
       write(30,*)"9"
    end do

    !write(30,*)" "
    !write(30,"(a,i3)")trim("POINT_DATA"),pn
    !write(30,"(a,1x,a,1x,a)")trim("SCALARS"),trim("point_scalar_data"),trim("float")
    !write(30,"(a,1x,a)")trim("LOOKUP_TABLE"),trim("default")
    !do i = 1 , pn
    !write(30,"(f10.5,1x,f10.5,1x,f10.5)")
    !enddo

   ! write(30,*)" "
   ! write(30,"(a,i6)")trim("CELL_DATA"),e
   ! write(30,"(a,1x,a,1x,a)")trim("SCALARS"),trim("error-map"),trim("float")
   ! write(30,"(a,1x,a)")trim("LOOKUP_TABLE"),trim("default")
   ! do i = 1 , e
   !   write(30,"(f10.5)")el2(i)
   ! enddo

    write(30,*)" "
    write(30,"(a,i6)")trim("POINT_DATA"),pn
    write(30,"(a,1x,a,1x,a)")trim("VECTORS"),trim("Displacement"),trim("float")
    do i = 1 , pn
      write(30,*)u(2 * i - 1), u(2 * i), 0.0d0
    enddo 

    write(30,*)" "
    write(30,"(a,i6)")trim("CELL_DATA"),e
    write(30,"(a,1x,a,1x,a)")trim("VECTORS"),trim("cell_vector_data"),trim("float")
    do i = 1,1728
    write(30,*)1.0d0 ,0.0d0,0.0d0
    enddo
    do i = 1729,e
    write(30,*)2.0d0,0.0d0,0.0d0
    enddo
    close (30)
  end subroutine print_text
    
subroutine Kesub(j,globalnum, n, y,p,d,jm,jmi,bm,Ke)
   implicit none
   integer,allocatable::globalnum(:,:)
   integer j,pn,ep,i,k,l,o
   real(8):: a, c, det, p, y
   real(8):: bdbdetp(8,8), bdb(8,8), db(3,8), jm(2,2), bt(8,3) = 0.0d0,&
             d(3,3), jmi(2,2), nxy(4,2) = 0.0d0, bm(3,8), Ke(8,8)
   real(8), allocatable ::n(:,:)
   
   Ke = 0.0d0
   do i = 1, 4
      bdbdetp = 0.0d0
      bdb = 0.0d0
      db = 0.0d0

      d(1,1) = y / (1.0d0 + p) / (1.0d0 - 2*p)*(1.0d0-p)
      d(1,2) = y / (1.0d0 + p) * p / (1.0d0 -2* p)
      d(1,3) = 0.0d0
      d(2,1) = y / (1.0d0 + p) * p / (1.0d0 - 2*p)
      d(2,2) = y / (1.0d0 + p) / (1.0d0 -2* p)*(1.0d0-p)
      d(2,3) = 0.0d0
      d(3,1) = 0.0d0
      d(3,2) = 0.0d0
      d(3,3) = 1.0d0 / 2.0d0 * y / (1.0d0 + p)

      if (i == 1)then
         a = -0.577350269189626d0
         c = -0.577350269189626d0
      else if (i == 2)then
         a = -0.577350269189626d0
         c = 0.577350269189626d0
      else if (i == 3)then
         a = 0.577350269189626d0
         c = 0.577350269189626d0
      else if (i == 4)then
         a = 0.577350269189626d0
         c = -0.577350269189626d0
      end if

      jm(1,1) = ((-1.0d0 + a) * n(globalnum(j,1),1) +(1.0d0 - a) * n(globalnum(j,2),1) +&
               (1.0d0 + a) * n(globalnum(j,3),1) + (-1.0d0 - a) * n(globalnum(j,4),1)) / 4.0d0
      jm(2,1) = ((-1.0d0 + c) * n(globalnum(j,1),1) +(-1.0d0 - c) * n(globalnum(j,2),1) +&
               (1.0d0 + c) * n(globalnum(j,3),1) + (1.0d0 - c) * n(globalnum(j,4),1)) / 4.0d0
      jm(1,2) = ((-1.0d0 + a) * n(globalnum(j,1),2) +(1.0d0 - a) * n(globalnum(j,2),2) +&
               (1.0d0 + a) * n(globalnum(j,3),2) + (-1.0d0 - a) * n(globalnum(j,4),2)) / 4.0d0
      jm(2,2) = ((-1.0d0 + c) * n(globalnum(j,1),2) +(-1.0d0 - c) * n(globalnum(j,2),2) +&
               (1.0d0 + c) * n(globalnum(j,3),2) + (1.0d0 - c) * n(globalnum(j,4),2)) / 4.0d0

      jmi(1,1) = jm(2,2) / (jm(1,1) * jm(2,2) - jm(1,2) * jm(2,1))
      jmi(1,2) = -jm(1,2) / (jm(1,1) * jm(2,2) - jm(1,2) * jm(2,1))
      jmi(2,1) = -jm(2,1) / (jm(1,1) * jm(2,2) - jm(1,2) * jm(2,1))
      jmi(2,2) = jm(1,1) / (jm(1,1) * jm(2,2) - jm(1,2) * jm(2,1))
   
      nxy(1,1) = (-(1.0d0 - a) * jmi(1,1) - (1.0d0 - c) * jmi(1,2))/ 4.0d0 
      nxy(1,2) = (-(1.0d0 - a) * jmi(2,1) - (1.0d0 - c) * jmi(2,2))/ 4.0d0 
      nxy(2,1) = ((1.0d0 - a) * jmi(1,1) - (1.0d0 + c) * jmi(1,2))/ 4.0d0 
      nxy(2,2) = ((1.0d0 - a) * jmi(2,1) - (1.0d0 + c) * jmi(2,2))/ 4.0d0
      nxy(3,1) = ((1.0d0 + a) * jmi(1,1) + (1.0d0 + c) * jmi(1,2))/ 4.0d0
      nxy(3,2) = ((1.0d0 + a) * jmi(2,1) + (1.0d0 + c) * jmi(2,2))/ 4.0d0
      nxy(4,1) = (-(1.0d0 + a) * jmi(1,1) + (1.0d0 - c) * jmi(1,2))/ 4.0d0
      nxy(4,2) = (-(1.0d0 + a) * jmi(2,1) + (1.0d0 - c) * jmi(2,2))/ 4.0d0
            
      bm(1,1) = nxy(1,1)
      bm(1,2) = 0.0d0
      bm(1,3) = nxy(2,1)
      bm(1,4) = 0.0d0
      bm(1,5) = nxy(3,1)
      bm(1,6) = 0.0d0
      bm(1,7) = nxy(4,1)
      bm(1,8) = 0.0d0
      bm(2,1) = 0.0d0
      bm(2,2) = nxy(1,2)
      bm(2,3) = 0.0d0
      bm(2,4) = nxy(2,2)
      bm(2,5) = 0.0d0
      bm(2,6) = nxy(3,2)
      bm(2,7) = 0.0d0
      bm(2,8) = nxy(4,2)
      bm(3,1) = nxy(1,2)
      bm(3,2) = nxy(1,1)
      bm(3,3) = nxy(2,2)
      bm(3,4) = nxy(2,1)
      bm(3,5) = nxy(3,2)
      bm(3,6) = nxy(3,1)
      bm(3,7) = nxy(4,2)
      bm(3,8) = nxy(4,1)

      do o = 1, 3
         do q = 1, 8
            bt(q,o) = bm(o,q) !btを定義
         end do
      end do
         
      do o = 1, 3
         do q = 1, 8
            do w = 1, 3
               db(o,q) = db(o,q) + d(o,w) * bm(w,q) !dbを計算
            end do
         end do
      end do
         
      do o = 1, 8
         do q = 1, 8
            do w = 1, 3
               bdb(o,q) = bdb(o,q) + bt(o,w) * db(w,q) !bdbを計算
            end do
         end do
      end do
 
      det = (jm(1,1) * jm(2,2)) - (jm(1,2) * jm(2,1)) !det計算

      if(det < 0) then
         write(*,*) "det<0"
         stop 
      end if
              
      do o = 1, 8
         do q = 1, 8
            bdbdetp(q,o) = bdb(q,o) * det !bdbdetp計算
         end do
      end do

      do k = 1,8
         do l = 1,8
            Ke(k,l) = Ke(k,l) + bdbdetp(k,l)
         end do
      end do
   end do
end subroutine Kesub

!!circular inclusion

subroutine gmshtofile(y1,y2,p1,p2,pn,e,ep,globalnum,x,y,r)
implicit none
character dummy
integer i,j,e,ep,bc1,bc2,bc3,bc0,s
integer,allocatable::globalnum(:,:)
integer(8) pn
real(8) alpha,lamda1,lamda2,mu1,mu2,sita,a,b,y1,p1,y2,p2
real(8),allocatable::x(:),y(:),ux(:),uy(:),ur(:),r(:)
a = 3.0d0
b = 15.0d0
pn = 10843
open(12, file="circular inclusion2", status="old")
    read (12, *)dummy
    read (12, *)dummy
    read (12, *)dummy
    allocate (x(pn),y(pn), source = 0.0d0)
    do i = 1, pn
       read(12,*)dummy,x(i),y(i),dummy
    end do
    read (12, *)dummy
    read (12, *)dummy
s = 1521  !1group element number
e = s * 7
ep = 4

allocate (globalnum(e,ep), source = 0)

do i = 1,s
 read (12, *)dummy,globalnum(i,1),globalnum(i,2),globalnum(i,3),globalnum(i,4)
 end do
read (12, *)dummy
do i = s + 1,s * 2
 read (12, *)dummy,globalnum(i,1),globalnum(i,2),globalnum(i,3),globalnum(i,4)
 end do
read (12, *)dummy
do i = s * 2 + 1,s*3
 read (12, *)dummy,globalnum(i,1),globalnum(i,2),globalnum(i,3),globalnum(i,4)
 end do
read (12, *)dummy
do i = s*3+1,s*4
 read (12, *)dummy,globalnum(i,1),globalnum(i,2),globalnum(i,3),globalnum(i,4)
 end do
read (12, *)dummy
do i = s*4+1,s*5
 read (12, *)dummy,globalnum(i,1),globalnum(i,2),globalnum(i,3),globalnum(i,4)
 end do
read (12, *)dummy
do i = s*5+1,s*6
 read (12, *)dummy,globalnum(i,1),globalnum(i,2),globalnum(i,3),globalnum(i,4)
 end do
read (12, *)dummy
do i = s*6+1,e
 read (12, *)dummy,globalnum(i,1),globalnum(i,2),globalnum(i,3),globalnum(i,4)
 end do
    close (12)

    open(30, file = "nodeinclusion.dat", status = "replace")
    write(30,*) pn
do i = 1, pn
write(30,*)x(i),y(i)
end do
    close (30)

    open(31, file = "eleminclusion.dat", status = "replace")
    write(31,*) e,ep
    do i = 1, e
    write(31,*)globalnum(i,1),globalnum(i,2),globalnum(i,3),globalnum(i,4)
    end do
    close (31)

    open(32, file = "bcinclusion.dat", status = "replace")

lamda1 = y1*p1 / (1.0d0 + p1)/(1.0d0-2*p1)
lamda2 = y2*p2 / (1.0d0 + p2)/(1.0d0-2*p2)
mu1 = y1 / 2.0d0 /(1.0d0 + p1)
mu2 = y2 / 2.0d0 /(1.0d0 + p2)
alpha = (lamda1 + mu1 + mu2) * b * b /((lamda2 + mu2) *a*a +(lamda1 + mu1)*(b*b - a*a) + mu2 *b*b)
allocate (r(pn),ur(pn),ux(pn),uy(pn),source = 0.0d0)

do i = 1, pn
   r(i) = sqrt(x(i) ** 2.0d0 + y(i) ** 2.0d0)
   if(r(i) < 3.001d0) then
      ur(i) = ((1.0d0 - b * b / a / a) * alpha + b * b / a / a) * r(i)
   else
      ur(i) = (r(i)-b*b/r(i))*alpha + b*b/r(i)
   end if
   sita = atan2(y(i), x(i))
   ux(i) = ur(i) * cos(sita)
   uy(i) = ur(i) * sin(sita)
end do

! write(*,*)a,b,alpha,mu1,mu2
bc1 = 0
bc2 = 0
bc3 = 0
    do i = 1,pn
      if(y(i) == 0.0d0 .and. r(i) < 14.99d0)then
         bc1 = bc1 + 1
      end if
      if(x(i) == 0.0d0 .and. r(i) < 14.99d0)then
         bc2 = bc2 + 1
      end if
      if(r(i) >= 14.99d0)then
         bc3 = bc3 + 1
      end if
    enddo

bc0 = bc1 + bc2 + bc3 * 2 
    write(32,*) bc0, 2
    
do i = 1, pn
if(x(i) == 0.0d0 .and. r(i) < 14.99d0)then
write(32,*) i, 1, 0.0d0
endif
if(y(i) == 0.0d0 .and. r(i) < 14.99d0 )then
write(32,*) i, 2, 0.0d0
endif
if(r(i) >= 14.99d0) then
write(32,*) i, 1, ux(i)
write(32,*) i, 2, uy(i) 
endif
end do

close (32)

end subroutine

subroutine error(n,pn,e,globalnum,u,el2)
implicit none

integer i,j,e,pn
integer,allocatable::globalnum(:,:)
real(8) r,det,ec,em,sita,a,c,a1,b1,y1,y2,xi,yi,p1,p2,el2
real(8) jm(2,2),lamda1,lamda2,mu1,mu2,alpha,uthr,uthx,uthy,uxfem,uyfem
real(8),allocatable:: n(:,:),u(:)!,el2(:)

em = 0.0d0
ec = 0.0d0
y1 = 10000.0d0
y2 = 100000.0d0
p1 = 0.25d0
p2 = 0.3d0
a1 = 3.0d0
b1 = 15.0d0

lamda1 = y1*p1 / (1.0d0 + p1)/(1.0d0-2.0d0*p1)
lamda2 = y2*p2 / (1.0d0 + p2)/(1.0d0-2.0d0*p2)
mu1 = y1 / 2.0d0 /(1.0d0 + p1)
mu2 = y2 / 2.0d0 /(1.0d0 + p2)
alpha = (lamda1 + mu1 + mu2) * b1 * b1 /((lamda2 + mu2) *a1*a1 +(lamda1 + mu1)*(b1*b1 - a1*a1) + mu2 *b1*b1)

!allocate(el2(e),source = 0.0d0)
do i = 1, e  !要素ループ
 !  em = 0.0d0
 !  ec = 0.0d0
   do j = 1,4  !積分点ループ
      if (j == 1)then
         a = -0.577350269189626d0
         c = -0.577350269189626d0
      else if (j == 2)then
         a = -0.577350269189626d0
         c = 0.577350269189626d0
      else if (j == 3)then
         a = 0.577350269189626d0
         c = 0.577350269189626d0
      else if (j == 4)then
         a = 0.577350269189626d0
         c = -0.577350269189626d0
      end if
      xi = ((1.0d0-c)*(1.0d0-a)*n(globalnum(i,1),1) + (1.0d0+c)*(1.0d0-a)*n(globalnum(i,2),1) + &
      (1.0d0+c)*(1.0d0+a)*n(globalnum(i,3),1) + (1.0d0-c)*(1.0d0+a)*n(globalnum(i,4),1)) / 4.0d0
      yi = ((1.0d0-c)*(1.0d0-a)*n(globalnum(i,1),2) + (1.0d0+c)*(1.0d0-a)*n(globalnum(i,2),2) + &
      (1.0d0+c)*(1.0d0+a)*n(globalnum(i,3),2) + (1.0d0-c)*(1.0d0+a)*n(globalnum(i,4),2)) / 4.0d0
      r = sqrt(xi ** 2.0d0 + yi ** 2.0d0)
      sita = atan2(yi, xi)
      if (r < 3.001d0) then  !!!中心からの距離で半径方向の理論解を分けて定義
         uthr = ((1.0d0 - b1 * b1 / a1 / a1) * alpha + b1 * b1 / a1 / a1) * r
      else
         uthr = (r-b1*b1/r)*alpha + b1*b1/r
      end if
      uthx = uthr * cos(sita) !x方向とy方向に分解
      uthy = uthr * sin(sita)

      uxfem = ((1.0d0-c)*(1.0d0-a)*u(globalnum(i,1)*2-1) + (1.0d0+c)*(1.0d0-a)*u(globalnum(i,2)*2-1) + &  !!積分点変位
      (1.0d0+c)*(1.0d0+a)*u(globalnum(i,3)*2-1) + (1.0d0-c)*(1.0d0+a)*u(globalnum(i,4)*2-1)) / 4.0d0
      uyfem = ((1.0d0-c)*(1.0d0-a)*u(globalnum(i,1)*2) + (1.0d0+c)*(1.0d0-a)*u(globalnum(i,2)*2) + &
      (1.0d0+c)*(1.0d0+a)*u(globalnum(i,3)*2) + (1.0d0-c)*(1.0d0+a)*u(globalnum(i,4)*2)) / 4.0d0

      jm(1,1) = ((-1.0d0 + a) * n(globalnum(i,1),1) +(1.0d0 - a) * n(globalnum(i,2),1) +&
      (1.0d0 + a) * n(globalnum(i,3),1) + (-1.0d0 - a) * n(globalnum(i,4),1)) / 4.0d0
      jm(2,1) = ((-1.0d0 + c) * n(globalnum(i,1),1) +(-1.0d0 - c) * n(globalnum(i,2),1) +&
      (1.0d0 + c) * n(globalnum(i,3),1) + (1.0d0 - c) * n(globalnum(i,4),1)) / 4.0d0
      jm(1,2) = ((-1.0d0 + a) * n(globalnum(i,1),2) +(1.0d0 - a) * n(globalnum(i,2),2) +&
      (1.0d0 + a) * n(globalnum(i,3),2) + (-1.0d0 - a) * n(globalnum(i,4),2)) / 4.0d0
      jm(2,2) = ((-1.0d0 + c) * n(globalnum(i,1),2) +(-1.0d0 - c) * n(globalnum(i,2),2) +&
      (1.0d0 + c) * n(globalnum(i,3),2) + (1.0d0 - c) * n(globalnum(i,4),2)) / 4.0d0

      det = (jm(1,1) * jm(2,2)) - (jm(1,2) * jm(2,1)) 
       ec = ec + ((uthx - uxfem) ** 2.0d0 + (uthy - uyfem) ** 2.0d0) * det  
       em = em + (uthx ** 2.0d0 + uthy ** 2.0d0) * det

   end do
!   el2(i) = sqrt(ec) / sqrt(em)
end do

el2 = sqrt(ec) / sqrt(em)

end subroutine


end module 