module Kmatrix
  use module
  implicit none

contains

subroutine Kmat(globalnum,n,e,y,p,f1,pn,ep,bc0,bc,jm,jmi,nxy,bm,bdbdetp,Ke,K)
  integer::  l, m, x1 = 0, y1 = 0, e, pn, f1, ep, bc0,i
  integer,allocatable::globalnum(:,:), bc(:,:)
  real(8) p,y
  real(8)::Ke(8,8), d(3,3),jm(2,2),jmi(2,2),nxy(4,2),bm(3,8),bdbdetp(8,8)
  real(8),allocatable::K(:,:), n(:,:), bc1(:)

allocate(K(pn * f1,pn * f1),source = 0.0d0)

do i = 1, e
  call Kesub(i,globalnum, n, y,p,jm,jmi,nxy,bm,bdbdetp,Ke)
   do l = 1, f1 * ep
      if (mod(l, f1) == 0) then
         y1 = f1 * globalnum(i,(l / f1))
      else if (mod(l, 2) == 1) then
         y1 = f1 * globalnum(i,(l + 1)/f1) - 1
      end if
      do m = 1, f1 * ep
         if (mod(m, f1) == 0) then
            x1 = f1 * globalnum(i,(m / f1))
         else if (mod(m, f1) == 1) then
            x1 = f1 * globalnum(i,(m + 1)/f1) - 1
         end if
         K(x1,y1) = K(x1,y1) + Ke(m,l)
      enddo
   enddo
enddo

do l = 1, bc0
   if (bc(l,2) == 1) then
   x1 = bc(l,1) * f1 - 1
   else if (bc(l,2) == 2) then
   x1 = bc(l,1) * f1
   end if

   do m = 1, f1 * pn
      if (m == x1) then
      K(x1,m) = 1.0d0
      else if (m /= x1) then
      K(x1,m) = 0.0d0
      end if
   end do
    do m = 1, f1 * pn
      if (m == x1) then
      K(m,x1) = 1.0d0
      else if (m /= x1) then
      K(m,x1) = 0.0d0
      end if
   end do
end do
  
  end subroutine
end module Kmatrix
  
