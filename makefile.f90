program main
use module
implicit none
integer sp1,sp2,sp3,c
 real(8),allocatable::x1(:,:),y1(:,:),x2(:,:),y2(:,:)
 real(8) a,b,r,y,p,sgm0

y = 20000.0d0
p = 0.3d0

a = 6.0d0
b = 7.0d0
r = 1.0d0

sp1 = 40
sp2 = 40
sp3 = 40
c = 3 !rのc+1倍の領域で細かいメッシュ

sgm0 = 10000.0d0

call makenode(a,b,r,sp1,sp2,sp3,c,x1,y1,x2,y2)
call makeelem(sp1,sp2,sp3)
call makebc(y,p,r,sgm0,sp1,sp2,sp3,x1,y1,x2,y2)

end program