program main
  use module
  implicit none
  call read_file1
  write(*, *) y, p

  call read_file2
  write(*, *) pn
  write(*, *) n(1,1), n(1,2)
  write(*, *) n(2,1), n(2,2)
  write(*, *) n(3,1), n(3,2)
  write(*, *) n(4,1), n(4,2)

  call read_file3
  write(*, *) e, ep
  write(*, *) ec

  call read_file4
  write(*, *) bc0, f1
  write(*, *) bc(1,1), bc(1,2), bc(1,3)
  write(*, *) bc(2,1), bc(2,2), bc(2,3)
  write(*, *) bc(3,1), bc(3,2), bc(3,3)
  write(*, *) bc(4,1), bc(4,2), bc(4,3)

  call read_file5
  write(*, *) ld0, f2
  write(*, *) ld(1,1), ld(1,2), ld(1,3)
  write(*, *) ld(2,1), ld(2,2), ld(2,3)
  
end program main
